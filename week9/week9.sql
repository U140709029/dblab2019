create view USACustomers as
select CustomerID,CustomerName,ContactName
from Customers
where country = "USA";

select * from USACustomers;

select * 
from USACustomers join Orders on USACustomers.CustomerID = Orders.CustomerID;

create or replace view products_below_avg_price as
select ProductID,ProductName,Price
from Products
where Price < 30;

select * 
from USACustomers join Orders on USACustomers.CustomerID = Orders.CustomerID
where OrderID in (
	select OrderID
    from OrderDetails join products_below_avg_price on OrderDetails.ProductID = products_below_avg_price.ProductID);
    
drop view USACustomers;



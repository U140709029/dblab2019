#1-What is the address, zipcode and zip4 of universities in Turkey at 2013?
select Address, ZIPCode , ZIP4 , Website
from Universities join Countries on University.UniversityID = Countries.CountryID
where Year = 2013 and CountryName = "Turkey";

#2-Which Unversities have teaching rating greater than 85 at 2012?
select UniversityName , University.UniversityID
from University join Ratings on University.UniiversityID = Ratings.UniversityID
where TeachingRating > 85 and Year = 2012;

#3-What is the number of Unversity for each country 2013?
select count(UniversityName)
from University join Countries on University.UniversityID = Countries.UniversityID
where Year = 2013 group by CountryName;

#4-What is the Sum of TotalScore for each country at 2013?
select sum(TotalScore),CountryName
from Ratings join Countries on Countries.UniversityID = Ratings.UniversityID
where Year = 2013 group by CountryName;

#5-Which Universities have TotalScore greater than 85 and Female Student Ratio 
#greater than 50 at 2012?
select *
from University join Ratings on University.UniiversityID = Ratings.UniversityID
where TotalScore > 85 and Year = 2012 and UniversityID in(
	select UniversityID
    from Students
    where FemaleStudents > 50);

#6-How many different cities into top rank 100 universities at 2012??
select distinct CityName
from Countries join Cities on Countries.CountryID = Cities.CountryID
where UniversityID in(
	select UniversityID
    from University join Rank on University.UniversityID = Rank.UniversityID
    where Rank < 100 and Year = 2012);

#7-Which UK Unversities have ResearchRating greater than 80 and CitationsRating greater than 80 at 2012 and 2013?
select UniversityName 
from University join Countries on University.UniversityID = Countries.CountryID
where CountryName = "United Kingdom" and UniversityID in (
	select UniversityID
    from Ratings
    where ResearchRating > 80 and CitationsRating > 80);
    
#8-Which Universities out of the rank list for 2012 to 2013?
select UniversityName
from University as Uni join Rank as R on Uni.UniversityID = R.UniversityID
where Year = 2012 and UniversityID not in(
	select UniversiyID
    from Rank
    where Year = 2013);
    
#9-Which Universities have maximum number of international student into top rank 20 
#at 2012?
select UniversityName
from University join Rank on University.UniversityID = Rank.UniversityID
where Rank between 1 and 20 and Year = 2012 and UniversityID in(
	select UniversityID
    from Students
    where max(InternationalStudents));

#10-What is the average TeachingRating For each USA cities at 2012?
select avg(TeachingRating),CityName
from University join Ratings on University.UniversityID = Ratings.UniversityID
where Year = 2012 and UniversityID in(
	select UniversityID
    from Countries join Cities on Countries.CountryID = Cities.CountryID
    where Year = 2012)group by CityName;











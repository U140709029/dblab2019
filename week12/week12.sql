call select_all_customers;

call getCustomersbyCity("Barcelona");

select * from Employees;

set @maxSalary = 0;
call maxSalary(@maxSalary);
select @maxSalary;

#select *
#from Employees
#where Salary = maxSalary; ##You can not write without "OUT" parameter

set @m_count = 0;
call countGender(@m_count,"M");

set @f_count = 0;
call countGender(@f_count,"F");

select @m_count,@f_count;